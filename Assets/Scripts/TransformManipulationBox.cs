﻿using System;
using UnityEngine;

public class TransformManipulationBox : MonoBehaviour
{
    private Bounds defaultBounds;
    private Vector3 defaultScale;
    [SerializeField] private Transform encompassedTransform;

    [SerializeField] private Draggable moveHandler;
    [SerializeField] private RotationHandlers rotationHandlers;

    [SerializeField] private ScaleHandlers scaleHandlers;
    [SerializeField] private Tubes tubesHandlers;

    private void Awake()
    {
        defaultBounds = new Bounds(transform.position, Vector3.zero);
        defaultScale = encompassedTransform.localScale;

        CalculateBounds(encompassedTransform);
    }

    private void Start()
    {
        scaleHandlers.Init();
        rotationHandlers.Init();
        tubesHandlers.Init();

        scaleHandlers.OnScaleChanged += OnScaleChanged;

        SetDefaultBounds(defaultBounds);

        moveHandler.OnDrag += OnDrag;
    }

    private void CalculateBounds(Transform transform)
    {
        if (encompassedTransform)
        {
            Renderer[] renderers = transform.gameObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer renderer in renderers)
                defaultBounds.Encapsulate(renderer.bounds);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0f, 0f, 1f, 0.49f);
        Gizmos.DrawCube(defaultBounds.center, defaultBounds.size);
    }

    private void SetDefaultBounds(Bounds bounds)
    {
        BoxCollider boxCollider = moveHandler.GetComponent<BoxCollider>();
        boxCollider.size = bounds.size;
        moveHandler.transform.position = bounds.center;

        scaleHandlers.SetDefaultBounds(bounds);
        rotationHandlers.SetDefaultBounds(bounds);
        tubesHandlers.SetDefaultBounds(bounds);
    }

    private void UpdateHandlers(Bounds bounds)
    {
        BoxCollider boxCollider = moveHandler.GetComponent<BoxCollider>();
        boxCollider.size = bounds.size;
        moveHandler.transform.position = bounds.center;

        scaleHandlers.SetTransform(bounds);
        rotationHandlers.SetTransform(bounds);
        tubesHandlers.SetTransform(bounds);
    }

    private void OnScaleChanged(Vector3 scale)
    {
        Bounds bounds = defaultBounds;
        Vector3 newSize = bounds.size;
        newSize.x *= scale.x;
        newSize.y *= scale.y;
        newSize.z *= scale.z;

        bounds.size = newSize;

        Vector3 newScale = defaultScale;
        newScale.x *= scale.x;
        newScale.y *= scale.y;
        newScale.z *= scale.z;

        encompassedTransform.localScale = newScale;

        UpdateHandlers(bounds);
    }

    private void OnDrag(object sender, DragEventArgs eventArgs)
    {
        transform.position = eventArgs.CurrentCursorPosition;
    }

    [Serializable]
    public class TransformHandlers
    {
        protected Bounds defaultBounds;
        [SerializeField] protected Transform parent;

        public virtual void Init()
        {
        }

        public void SetDefaultBounds(Bounds bounds)
        {
            defaultBounds = bounds;
            if (parent)
                parent.localPosition = bounds.center;
            CoreSetTransform(bounds);
        }

        public void SetTransform(Bounds bounds)
        {
            CoreSetTransform(bounds);
        }

        protected virtual void CoreSetTransform(Bounds bounds)
        {
        }
    }

    [Serializable]
    public class ScaleHandlers : TransformHandlers
    {
        [SerializeField] private ScaleHandler[] handlers;

        private Vector3 scale = Vector3.one;

        public event Action<Vector3> OnScaleChanged;


        public override void Init()
        {
            foreach (ScaleHandler handler in handlers)
            {
                handler.Init(this);
                handler.OnDragEvent += OnDrag;
            }
        }

        public void SetScale(Vector3 value)
        {
            if (scale != value)
            {
                scale = value;

                if (OnScaleChanged != null) OnScaleChanged.Invoke(value);
            }
        }

        protected override void CoreSetTransform(Bounds bounds)
        {
            base.CoreSetTransform(bounds);

            foreach (ScaleHandler handler in handlers)
                handler.SetTransform(bounds);
        }

        protected void OnDrag(object sender, DragEventArgs eventArgs)
        {
            ScaleHandler scaleHandler = sender as ScaleHandler;

            Vector3 defaultTranlation = scaleHandler.DefaultPosition - defaultBounds.center;
            Vector3 newTranslation = scaleHandler.DraggableComp.transform.position - defaultBounds.center;

            Vector3 newScale = new Vector3(
                newTranslation.x / defaultTranlation.x,
                newTranslation.y / defaultTranlation.y,
                newTranslation.z / defaultTranlation.z);

            SetScale(newScale);
        }

        [Serializable]
        public class ScaleHandler
        {
            private ScaleHandlers controller;

            [SerializeField] private Draggable draggable;
            [SerializeField] private bool forward;
            [SerializeField] private bool right;
            [SerializeField] private bool top;

            public bool Top => top;

            public bool Right => right;

            public bool Forward => forward;

            public Draggable DraggableComp => draggable;

            public Vector3 DefaultPosition
            {
                get
                {
                    Bounds bounds = controller.defaultBounds;
                    Vector3 center = bounds.center;
                    Vector3 halfSize = bounds.size / 2;

                    Vector3 position = center;
                    position.x += right ? halfSize.x : -halfSize.x;
                    position.y += top ? halfSize.y : -halfSize.y;
                    position.z += forward ? halfSize.z : -halfSize.z;

                    return position;
                }
            }

            public event DragEventHandler OnDragEvent;


            public void Init(ScaleHandlers handlers)
            {
                controller = handlers;
                draggable.OnDrag += OnDrag;
            }

            private void OnDrag(object sender, DragEventArgs drag)
            {
                draggable.transform.position = drag.LastCursorPosition;

                if (OnDragEvent != null)
                    OnDragEvent.Invoke(this, drag);
            }

            public void SetTransform(Bounds bounds)
            {
                Vector3 center = bounds.center;
                Vector3 halfSize = bounds.size / 2;

                Vector3 position = center;
                position.x += right ? halfSize.x : -halfSize.x;
                position.y += top ? halfSize.y : -halfSize.y;
                position.z += forward ? halfSize.z : -halfSize.z;

                draggable.transform.position = position;
            }
        }
    }

    [Serializable]
    public class RotationHandlers : TransformHandlers
    {
        [SerializeField] private Draggable bottom_forward_midRight;

        [SerializeField] private Draggable bottom_midForward;
        [SerializeField] private Draggable bottom_midRight;
        [SerializeField] private Draggable bottom_right_midForward;
        [SerializeField] private Draggable middle;
        [SerializeField] private Draggable middle_forward;
        [SerializeField] private Draggable middle_forward_right;
        [SerializeField] private Draggable middle_right;

        private Vector3 rotation = Vector3.zero;
        [SerializeField] private Draggable top_forward_midRight;

        [SerializeField] private Draggable top_midForward;
        [SerializeField] private Draggable top_midRight;
        [SerializeField] private Draggable top_right_midForward;

        protected override void CoreSetTransform(Bounds bounds)
        {
            base.CoreSetTransform(bounds);

            Vector3 halfBounds = bounds.size / 2;
            Vector3 center = bounds.center;

            Vector3 floorPosition = center;
            Vector3 position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = floorPosition.z - halfBounds.z;
            middle.transform.position = position;

            position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = floorPosition.z + halfBounds.z;
            middle_forward.transform.position = position;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = floorPosition.z - halfBounds.z;
            middle_right.transform.position = position;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = floorPosition.z + halfBounds.z;
            middle_forward_right.transform.position = position;

            floorPosition = center;
            floorPosition.y = center.y + halfBounds.y;

            position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = center.z;
            top_midForward.transform.position = position;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = center.z;
            top_right_midForward.transform.position = position;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z - halfBounds.z;
            top_midRight.transform.position = position;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z + halfBounds.z;
            top_forward_midRight.transform.position = position;

            floorPosition = center;
            floorPosition.y = center.y - halfBounds.y;

            position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = center.z;
            bottom_midForward.transform.position = position;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = center.z;
            bottom_right_midForward.transform.position = position;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z - halfBounds.z;
            bottom_midRight.transform.position = position;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z + halfBounds.z;
            bottom_forward_midRight.transform.position = position;
        }


        public void ChangeRotationX(float value)
        {
        }

        public void ChangeRotationY(float value)
        {
        }

        public void ChangeRotationZ(float value)
        {
        }
    }

    [Serializable]
    public class Tubes : TransformHandlers
    {
        [SerializeField] private Transform bottom_forward_midRight;

        [SerializeField] private Transform bottom_midForward;
        [SerializeField] private Transform bottom_midRight;
        [SerializeField] private Transform bottom_right_midForward;
        [SerializeField] private Transform middle;
        [SerializeField] private Transform middle_forward;
        [SerializeField] private Transform middle_forward_right;
        [SerializeField] private Transform middle_right;
        [SerializeField] private Transform top_forward_midRight;

        [SerializeField] private Transform top_midForward;
        [SerializeField] private Transform top_midRight;
        [SerializeField] private Transform top_right_midForward;

        protected override void CoreSetTransform(Bounds bounds)
        {
            base.CoreSetTransform(bounds);

            Vector3 halfBounds = bounds.size / 2;
            Vector3 center = bounds.center;
            Vector3 scale = Vector3.one;
            scale.y = bounds.size.y;

            //--------------- MIDDLE ---------------
            Vector3 floorPosition = center;
            Vector3 position = floorPosition;


            position.x = floorPosition.x - halfBounds.x;
            position.z = floorPosition.z - halfBounds.z;
            middle.transform.position = position;
            middle.transform.localScale = scale;

            position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = floorPosition.z + halfBounds.z;
            middle_forward.transform.position = position;
            middle_forward.transform.localScale = scale;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = floorPosition.z - halfBounds.z;
            middle_right.transform.position = position;
            middle_right.transform.localScale = scale;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = floorPosition.z + halfBounds.z;
            middle_forward_right.transform.position = position;
            middle_forward_right.transform.localScale = scale;

            // ------------------ TOP -------------------
            floorPosition = center;
            floorPosition.y = center.y + halfBounds.y;

            scale = Vector3.one;
            scale.z = bounds.size.z;

            position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = center.z;
            top_midForward.transform.position = position;
            top_midForward.transform.localScale = scale;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = center.z;
            top_right_midForward.transform.position = position;
            top_right_midForward.transform.localScale = scale;

            scale = Vector3.one;
            scale.x = bounds.size.x;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z - halfBounds.z;
            top_midRight.transform.position = position;
            top_midRight.transform.localScale = scale;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z + halfBounds.z;
            top_forward_midRight.transform.position = position;
            top_forward_midRight.transform.localScale = scale;


            //--------------- BOTTOM ---------------
            floorPosition = center;
            floorPosition.y = center.y - halfBounds.y;

            scale = Vector3.one;
            scale.z = bounds.size.z;

            position = floorPosition;
            position.x = floorPosition.x - halfBounds.x;
            position.z = center.z;
            bottom_midForward.transform.position = position;
            bottom_midForward.transform.localScale = scale;

            position = floorPosition;
            position.x = floorPosition.x + halfBounds.x;
            position.z = center.z;
            bottom_right_midForward.transform.position = position;
            bottom_right_midForward.transform.localScale = scale;

            scale = Vector3.one;
            scale.x = bounds.size.x;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z - halfBounds.z;
            bottom_midRight.transform.position = position;
            bottom_midRight.transform.localScale = scale;

            position = floorPosition;
            position.x = center.x;
            position.z = floorPosition.z + halfBounds.z;
            bottom_forward_midRight.transform.position = position;
            bottom_forward_midRight.transform.localScale = scale;
        }
    }
}